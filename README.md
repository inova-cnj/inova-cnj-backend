# Inova CNJ Backend

Especificação TEAM 42

Projeto de uma api rest com métodos que atendam as demandas do desafio CNJ inova baseado principalmente em IA

BD

Os arquivos de dados fornecidos pelo desafio foram transformados em um banco de dados relacional Postgresql, já que é o mais utilizado nos tribunais brasileiros. Foram desenvolvidos scripts de importação dos dados em python e php.  Para uma maior performance, foram criados índices baseados nos filtros realizados pela aplicação. Com o foco do trabalho em análise de tempo de movimentações e gargalos, foi desenvolvido um script para preparar os dados de tempo de maneira paralela. Com esses ajustes as funções matemáticas para os cálculos usados na aplicação se tornaram mais eficientes. Por fim foi gerado e disponibilizado um dump da base de dados na raiz do projeto backend.

Backend

Feito em python, o backend utiliza técnicas de estatística e manipulação de Dataframes para gerar dados que serão enviados ao frontend para então serem visualizados pelo usuário. A estrutura do código facilita a adição de novos métodos e reports estatísticos, mudanças nas complexidades dos filtros de consulta, e um simples acoplamento em sistemas de dados já existentes. 
