import requests
import json
from _datetime import datetime


class Notifications:

    @staticmethod
    def notify_success(job_name):
        requests.post(
            url='https://hooks.slack.com/services/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',
            data=json.dumps({
                'text': 'Rodando o {}: {}'.format(job_name, datetime.now()),
                'channel': '#ia-jobs'
            })
        )

    @staticmethod
    def notify_error(job_name, error=None):
        requests.post(
            url='https://hooks.slack.com/services/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',
            data=json.dumps({
                'text': 'Erro no job {}: `\n{}`'.format(job_name, error),
                'channel': "#ia-erros"
            })
        )
